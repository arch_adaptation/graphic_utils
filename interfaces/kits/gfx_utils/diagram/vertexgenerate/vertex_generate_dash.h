/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef GRAPHIC_LTE_VERTEX_GENERATE_DASH_H
#define GRAPHIC_LTE_VERTEX_GENERATE_DASH_H

#include "gfx_utils/diagram/common/common_basics.h"
#include "gfx_utils/diagram/vertexprimitive/geometry_vertex_sequence.h"

namespace OHOS {
/**
 * @brief dash Segment generator
 * @since 1.0
 * @version 1.0
 */
class VertexGenerateDash {
};
} // namespace OHOS

#endif
